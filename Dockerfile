# 從哪裡開始
FROM denoland/deno:alpine-1.26.0
# 建立資料夾
WORKDIR /app
# 複製檔案
COPY . /app
# 預計會執行的 port
EXPOSE 8000
# 執行指令
RUN deno cache main.ts

CMD ["run", "--allow-all", "main.ts"]